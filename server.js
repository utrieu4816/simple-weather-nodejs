const http = require('http')
const fs = require('fs')
const url = require('url')
const axios = require('axios')

const api_key = "bf32061352e6df02411a3226ec255c0f";
const port = process.env.port || 8000


let handleRequest = (req, res) => {
    const query = url.parse(req.url, true).query;
    if(query.city === undefined){
        res.writeHead(200, {
            'Content-Type': 'text/html'
        })
    
        fs.readFile('./index.html', null, (error, data) => {
            if(error){
                res.writeHead(404)
                res.write('File not found')
            } else {
                res.write(data)
            }
            res.end()
        })
    }
    else{
        try{
            axios(`http://api.openweathermap.org/data/2.5/weather?q=${query.city},ca&appid=${api_key}`).then(resp => {
                let temp = Math.round(resp.data.main.temp - 273.15);
                let feelsLike = Math.round(resp.data.main.feels_like - 273.15)
                let minTemp = Math.round(resp.data.main.temp_min - 273.15)
                let maxTemp = Math.round(resp.data.main.temp_max - 273.15)
                let message = `${query.city}: ${resp.data.weather[0].description}\n` + 
                    `Temp: ${temp}C - Feels like: ${feelsLike}C - Min: ${minTemp}C - Max: ${maxTemp}\n`
                res.writeHead(200, {
                    'Content-Type': 'text/plain'
                })
                res.write(message)
                res.end()
            }).catch(error => {
                res.write("Something went wrong")
                res.end()
            })
        }
        catch(error){
            res.write("Something went wrong")
            res.end()
        }
    }
}

http.createServer(handleRequest).listen(port)